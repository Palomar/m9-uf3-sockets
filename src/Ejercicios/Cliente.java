package Ejercicios;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Cliente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Cliente que enviar� el archivo designado en path
		try{
			Socket soc = new Socket("localhost",4100);
			PrintStream enviar = new PrintStream(soc.getOutputStream());
			String path = "src/w.png";
			System.out.println("La ruta donde enviamos es: "+path);
			FileInputStream origen = new FileInputStream(path);
			
			byte[] buffer = new byte[1024];
			int len;
			
			while((len=origen.read(buffer))>0){
				enviar.write(buffer,0,len);
			}
			
			soc.close();
			enviar.close();
			origen.close();
			
		}catch(IOException e){e.printStackTrace();}
	}

}
