package Ejercicios;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Se ejecuita antes que Cliente.java, escuchar� la conexi�n que se realice y rehar� el archivo de imagen enviado
		try{
			ServerSocket Ssoc = new ServerSocket(4100);
			//String path = "src/w.jpg";
			//String path = "D:\\JORDI\\M9-SOCOLS\\M9-UF3-ZocalosEjercicios\\src\\recibe.png";
			String path = "src/recibe.png";
			System.out.println("La ruta es: "+path);
			Socket soc = Ssoc.accept();
			InputStream arribada = soc.getInputStream();
			FileOutputStream desti = new FileOutputStream(path);
			
			byte[] buffer = new byte[1024];
			int len;
			while((len=arribada.read(buffer))>0){
				desti.write(buffer,0,len);
			}
			Ssoc.close();
			arribada.close();
			desti.close();
			soc.close();
		}catch(IOException e){e.printStackTrace();}
	}

}
