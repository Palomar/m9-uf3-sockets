# Aplicaci�n de Z�calos

Proyecto realizado en el marco del M�dulo 09 UF3 sobre sockets. Se realiz� una propuesta de aplicaci�n que trabajase con z�calos. Dicha propuesta consist�a en una aplicaci�n de sala de chat inspirada en mIRC con la posibilidad de que distintos usuarios con diferentes computadoras, pudiera conversar y a la vez participar en un juego de trivial.

A su vez se propuso el reto de realizarla con multicast y UDP a diferencia del resto de compa�eros para demostrar que era viable, aunque con limitaciones.

Este proyecto fue realizado por dos alumnos del IES Sabadell.

## Instrucciones

Se recomienda ejecutar desde un Eclipse Neon compatible con JavaFX, si no dispones de la tecnolog�a JavaFX en tu entorno de desarrollo, procede a su instalaci�n primero ya que la interfaz del programa se ejecuta en dicha tecnolog�a.

1. Importa todo el proyecto dentro de tu IDE, ten como referencia la clase **MainApp.java** del paquete **interfaz** para ejecutar la aplicaci�n.
El paquete **Practica** es el backend.

2. Para garantizar el correcto funcionamiento del hilo de servidor, primero debes arreglar la tabla de enrutamiento dado que el programa se dise�� para operar con un hilo servidor multicast, con clientes que env�an por v�a UDP.
Para realizar dicho cambio primero debes observar tu tabla y ver tu IP de multicast, abre la consola del sistema y teclea: **route print**

Nos interesa las que se indican en el rango que tiene por m�scara 240.0.0.0 que son las de multicasting. Si no os aparece como primera opci�n vuestra IP se deber� configurar, para ello usaremos la siguiente instrucci�n:

**route change 224.0.0.0 mask 240.0.0.0 <nuestra ip> metric <entero>**

Donde <nuestra ip> figura nuestra IP que identifica nuestra m�quina en la red y en <entero> de metric figuraremos una m�trica que sea inferior a la que la primera ip (por lo general 127.0.0.1) ten�a en su lugar.
De esta forma lograremos sobrescribir la tabla de enrutamiento y a partir de entonces se podr� escuchar el multicast que env�a el servidor desde distintos nodos.

![Ejemplo](/imagen.png)

3. Cuando se ejecute el programa en una m�quina, se debe arrancar el servidor �nicamente en aquella m�quina que se encargar� de hacer de servidor. La m�quina que hace de servidor tambi�n puede arrancar una sala de chat como cualquier otro usuario.

4. La IP que debe introducir el usuario que se conecta a una sala de chat es la IP de que dispone en la red la m�quina que hace de servidor.

## Informe descriptivo

### Funcionamiento b�sico

Nuestra aplicaci�n es b�sicamente una pantalla desde la que se puede arrancar el servidor y hasta conectarse con un cliente. O simplemente conectarse a un servidor ajeno. La funcionalidad principal es la de enviar mensajes mediante UDP y con Multicast, el servidor env�a esos mensajes a los usuarios conectados para que los pueda ver todo el mundo. Es un chat global. 
Para iniciar como cliente, el programa pide la direcci�n IP del servidor a la que hay que conectarse y un Nick. El Nick no podr� ser �master� ya que est� reservado para el servidor.

Nada m�s iniciar el servidor comenzar� una cuenta atr�s para que los usuarios puedan inscribirse a la partida de �Trivial� solo con escribir �!participar�, la cual dura 10 segundos. Una vez comenzada la partida se avisar� a todos los clientes quien participa e iniciar�n las preguntas. Los participantes deber�n responder las preguntas en un tiempo, o hasta que alguien acierte. Hasta entonces los participantes podr�n escribir tantos intentos como quieran. Los fallos restan 1 punto y los aciertos suman 3. Los clientes que est�n conectados, pero no participen, podr�n chatear con normalidad, no se les expulsar�. 

El servidor notificar� despu�s de cada pregunta del usuario que la haya acertado si es el caso, y seguidamente la respuesta correcta.

Una vez finalizada la partida, el servidor notificar� qu� usuario ha ganado y la puntuaci�n que ha logrado.

### Idea principal

La idea principal de la pr�ctica era implementar un servicio de chat por Multicast en el que se pudiera participar en un juego de preguntas. Tambi�n ten�amos en mente integrarle interfaz gr�fica, un chat privado entre usuarios mediante TCP y la no repetici�n de Nicks.

### Dificultades

Solo hemos encontrado un par de dificultades, aunque una de ellas nos ha fastidiado una idea principal de la aplicaci�n que ten�amos en mente.

La primera de ellas fue que al enviar el paquete por UDP, el servidor recib�a una serie de bytes rellenados con un s�mbolo por defecto. Por eso, al intentar hacer �append� a la String nos parec�a que no sal�a nada por consola, pero lo que en realidad pasaba es que aparec�a muy espaciado hacia la derecha. Nos dio muchos quebraderos de cabeza dar con esa tonter�a.

La siguiente dificultad, la cual no pudimos solventar, fue que intentamos mandar desde el servidor un mensaje v�a TCP al cliente que estaba intentando conectar con un Nick repetido para hacerle escribir de nuevo el Nick. Conseguimos hacer que se enviara el mensaje hasta una vez, pero cuando el usuario intentaba poner el Nick repetido por segunda vez, suced�a un fallo con la conexi�n TCP. A su vez, si se introduc�a un Nick correcto, el mensaje de admisi�n lo recib�a un usuario que no correspond�a con el que enviaba el mensaje. Al no saber de d�nde proced�a ese curioso error, decidimos echarnos para atr�s, tanto en la repetici�n de Nicks, como en el chat privado entre usuarios, vista la incompatibilidad de las conexiones UDP con TCP y dado el tiempo que nos quedaba para implementar la interfaz gr�fica.

### Ampliaciones

Para desarrollar la interfaz primero se dise�aron las tres pantallas por las que podr�a interactuar el usuario, es decir los tres archivos fxml que iban a ser usados m�s el fxml de men�: el de pantalla inicial donde se mostrar� el registro de mensajes del servidor, el de inicio donde se introducen direcci�n IP y nick, y finalmente el de interfaz del canal de mensajes donde se deb�an ubicar el registro de mensajes, el formulario para enviar mensajes y a la derecha la lista de participantes del canal.

Una vez se dise�� esta parte con el Scenebuilder de JavaFX 8, el siguiente paso fue incorporar la versi�n de consola a la interfaz. En un principio se quiso crear el servidor desde MainApp como un objeto que pudiera ejecutarse ah�, pero debido a la concepci�n de c�mo funciona el c�digo con JavaFX 8 fue inviable, as� que se tuvo que crear un objeto hilo de la clase ServerUDP dentro del paquete Hilo de forma que se pudiera ejecutar paralelamente sin afectar el flujo de ejecuci�n de la interfaz. Para lograr incrustar los mensajes salientes del ServerUDP en la interfaz, se cre� un m�todo en MainApp que hace referencia a un m�todo del controlador del men� (RootLayoutController.java) de forma que cuando se crea e incorpora el fxml de men� al espacio de la interfaz del MainApp con el fxml del InitOverview, se le pasa la referencia al RootLayout.fxml, logrando acceso a un m�todo que hace append sobre el campo textarea que tiene la propiedad id fxml.

Este m�todo se aplica tambi�n en la interfaz para inyectar los mensajes recibidos del ServerUDP a la clase ClientMultiCast, la clase que se encarga de leer los mensajes multicast de la primera, con el ChatOverviewController.

A parte del reto de integrar lo hecho por consola sin que hubiese que manipularlo demasiado, surgieron algunos detalles que por consola no se tuvieron en cuenta. Por ejemplo el de la lista de nicks en la interfaz era algo no previsto cuando se codific� la versi�n por consola, as� que hubo que realizar un mensaje especial de multicast con un determinado formato que el cliente sabe interpretar y desgajar en una lista de tipo Alias que luego incorpora como Observable List dentro de la tabla.

Otro problema hallado fue la codificaci�n de los mensajes mostrados dentro de la interfaz. Como estuvimos usando mensajes de prueba en el fichero CSV que no ten�an incorporados las tildes, al crear un CSV con preguntas reales surgi� dicho problema, unido tambi�n a algunos mensajes internos de usuarios e hilos que fallaban en algunos caracteres no US-ASCII. As� que hubo que realizar pruebas de codificaci�n UTF8 y descodificaci�n UTF8 con los mensajes enviados por UDP y recibidos por UDP, as� como los enviados por multicast y recibidos por multicast, y finalmente sobre el m�todo de lectura del archivo CSV que se tuvo que encapsular en una InputStreamer para a�adirle la codificaci�n de lectura UTF8 del archivo.

Ya en la fase final decidimos a�adirle un par de gifs animados en la pantalla principal para hacerlo m�s visual y m�sica de fondo, as� como un sonido cada vez que se recibe un mensaje.

### Conclusiones

El haber apostado por el uso de UDP y Multicast nos ha servido para comprobar que s� es posible hacer un prototipo de aplicaci�n de mensajes utilizando esos z�calos, pero tambi�n para aprender que aunque fuera f�cil y r�pido el crear la primera funcionalidad por consola para enviarse mensajes, con posterioridad ha dado bastantes dolores de cabeza algunos retos como incorporar TCP para crear mensajes privados y asegurar la no repetici�n de nicks, algo a lo que tuvimos que renunciar por hallar diversos errores tales como que un mensaje de bienvenida enviado a una IP por TCP le llegase tambi�n a otros usuarios que no compart�an dicha IP en otra m�quina, como si al existir el multicast de por medio estuviese interfiriendo en la comunicaci�n, a pesar de estar en puertos distintos.

En cambio cuestiones como a�adir im�genes en movimiento, m�sica o sonido, han sido realmente much�simo m�s f�ciles de incorporar de lo esperado gracias a JavaFX 8, cuando desde un principio no contempl�bamos incorporarlas.

La inversi�n de horas no se ha contabilizado para la parte backend de la aplicaci�n, pero la interfaz ha conllevado alrededor de 10 horas, siendo la mayor parte de ellas invertidas en integrarla con los hilos y los dos problemas mencionados arriba.

No se ha tenido en cuenta el incorporar un sistema de archivos por categor�a, lee solamente un archivo CSV desde el cual extrae todas las preguntas, las procesa orden�ndolas aleatoriamente sin repetir y genera una lista de 5 preguntas. El m�todo reiniciarTest(int) de ServerUDP recibe el n�mero de preguntas total que se quiere para el trivial y lo crea gracias a que el m�todo cargarPreguntas() hizo la lectura del archivo a la memoria del programa. No se incorpor� un sistema de categor�as ya que hemos decidido cerrar la aplicaci�n para invertir tiempo en otros proyectos, pero no es un reto que sea dif�cil de implementar y es meramente un a�adido m�s que con tiempo se puede hacer, consideramos m�s fundamental lo presentado donde se puede observar el trabajo real de comunicaci�n de z�calos y su integraci�n en una interfaz.